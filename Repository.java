package pakiet;

import javax.management.Query;

public class Repository {

	
	public interface IClientRepository
	{    
	    TvSeries GetById(Guid id);   
	    TvSeries GetByQuery(Query query);
	    void Add(TvSeries tvseries);
	    void Remove(TvSeries tvseries);
	    void Update(TvSeries tvseries);    
	       
	}
}

