package pakiet;


import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
 
import eu.mySQL.jdbc.ConfigurationException;
 
public class DbConnection {
 
    private static String connectionURI;
    private static boolean isPrepared = false;
    private static String connectionClass;
 
    private static void prepare() throws ConfigurationException {
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream("db.configuration"));
        } catch (FileNotFoundException e) {
            throw new ConfigurationException(e);
        } catch (IOException e) {
            throw new ConfigurationException(e);
        }
        if (properties.containsKey("connection.uri")) {
            connectionURI = properties.getProperty("connection.uri");
        } else {
            throw new ConfigurationException("brak klucza connection.url");
        }
        if (properties.containsKey("connection.class")) {
            connectionClass = properties.getProperty("connection.class");
        } else {
            throw new ConfigurationException("brak klucza connection.class");
        }
        try {
            Class.forName(connectionClass);
        } catch (ClassNotFoundException e) {
            throw new ConfigurationException("brak sterownika " + connectionClass);
        }
        isPrepared = true;
    }
 
    public static Connection getConnection() throws ConfigurationException {
        if (!isPrepared)
            try {
                prepare();
            } catch (ConfigurationException e) {
                e.printStackTrace();
            }
        try {
            Connection connection = DriverManager.getConnection(connectionURI);
            return connection;
        } catch (SQLException e) {
            throw new ConfigurationException(e);
        }
    }
 
